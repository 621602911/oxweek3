/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.captain.week3;

import java.util.*;
import java.util.Scanner;

/**
 * @author captain
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game....");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Plese input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }

            System.out.println("Error");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();           
            table.checkWin();
            if(table.isFinish()){
                if(table.getWinner()==null){
                    this.showTable();
                    System.out.println("Draw!!");
                }else{
                    this.showTable();
                    System.out.println("Player "+table.getWinner().getName()+" Win!!");
                    System.out.println("Bye bye...");
                }
                break;
            }
            table.switchPlayer();
        }
    }
}
